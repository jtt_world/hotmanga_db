# -*- coding: utf-8 -*-
"""
    @author: jtt  
    @file: util_upload.py
    @time: 2018/6/27 11:29
    @desc:
"""
import os
import uuid

ALIYUN_USER_SPACE = "user/"
ALIYUN_SUBJECT_SPACE = "subject/"
ALIYUN_AUTHOR_SPACE = "author/"
ALIYUN_LOCAL_SPACE = "local/"
ALIYUN_COMIC_SPACE = "comic/"
ALIYUN_COMIC_THEME_SPACE = "theme/"
ALIYUN_RECOMMEND_SPACE = "recommend/"

PATTERN_IMAGE = '^[(.JPG)|(.PNG)|(.GIF)]+$'

IMAGE_UPLOAD_SIZE = 100 * 1024 * 1024
IMAGE_USER_UPLOAD_SIZE = 5 * 1024 * 1024


# multipart/form-data
# from django import forms
#
# class UploadFileForm(forms.Form):
#     title = forms.CharField(max_length=50)
#     file = forms.FileField()
#
# from django.http import HttpResponseRedirect
# from django.shortcuts import render_to_response
# from somewhere import handle_uploader_file
#
# def upload_file(request):
#   if request.method == 'POST':
#      form = UploadFileForm(request.POST, request.FILES)
#      if form.is_valid():
#         handle_uploaded_file(request.FILES['file'])
#         return HttpResponseRedirect('/success/url')
#   else:
#      form = UploadFileForm()
#   return render_to_response('upload.html', {'form': form})

# 参数不固定 是为了防止cdn缓存

def user_avatar_upload_to(instance, filename):
    (filename, suffix) = os.path.splitext(filename)
    return '%s%s/%s/%s%s' % (ALIYUN_USER_SPACE, instance.uuid, 'avatar', uuid.uuid1(), suffix)


def author_upload_to(instance, filename):
    (filename, suffix) = os.path.splitext(filename)
    return '%s%s/%s%s' % (ALIYUN_AUTHOR_SPACE, instance.path_word, uuid.uuid1(), suffix)


def local_upload_to(instance, filename):
    (filename, suffix) = os.path.splitext(filename)
    return '%s%s/%s%s' % (ALIYUN_LOCAL_SPACE, instance.path_word, uuid.uuid1(), suffix)


def chapter_upload_to(instance, filename):
    chapter = instance.chapter
    comic = chapter.comic
    (filename, suffix) = os.path.splitext(filename)
    return '%s%s/%s/%s%s' % (ALIYUN_COMIC_SPACE, comic.path_word, chapter.serial, uuid.uuid1(), suffix)


def comic_upload_to(instance, filename):
    (filename, suffix) = os.path.splitext(filename)
    return '%s%s/%s/%s%s' % (ALIYUN_COMIC_SPACE, instance.path_word, "cover", uuid.uuid1(), suffix)


def theme_upload_to(instance, filename):
    (filename, suffix) = os.path.splitext(filename)
    return '%s%s%s' % (ALIYUN_COMIC_THEME_SPACE, uuid.uuid1(), suffix)


def recommend_upload_to(instance, filename):
    (filename, suffix) = os.path.splitext(filename)
    return '%s%s%s' % (ALIYUN_RECOMMEND_SPACE, uuid.uuid1(), suffix)
