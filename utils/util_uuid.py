# -*- coding: utf-8 -*-
"""
    @author: jtt  
    @file: util_uuid.py
    @time: 2018/6/28 11:08
    @desc: 这是表主键生成,目前使用uuid1(), 不安全可以统一替换掉
"""
import hashlib
import random
import uuid


def get_prim():
    """
    获取主键
    :return:
    """
    return uuid.uuid1()


def get_md5(url):
    if isinstance(url, str):
        url = url.encode("utf-8")
    m = hashlib.md5()
    m.update(url)
    return m.hexdigest()


def get_5_16_number():
    """
    获取主键
    :return:
    """
    info = "1234567890abcdef"
    return "".join(random.sample(info, 5))
