# -*- coding: utf-8 -*-
"""
    @author: jtt  
    @file: model_const.py
    @time: 2018/6/27 11:29
    @desc: 类type属性
"""

from enum import Enum


class EnumUserBlood(Enum):
    A = "A"
    B = "B"
    AB = "AB"
    O = "O"


class TagGenderType(Enum):
    Male = 1  # '男性标签'
    Female = 2  # '女性标签'


class EnumVipType(Enum):
    Normal = 0  # '普通会员'
    Pay = 1  # '付费会员'


class EnumOrderStatus(Enum):
    Unpaid = 0  # '未支付'
    Paid = 1  # '已支付'
    Error = 2  # '支付失败'


class EnumOrderPayPlatform(Enum):
    Other = 0  # '暂时其他平台'
    AliPay = 1  # '支付宝'
    WX = 2  # '微信'


class EnumRecommendType(Enum):
    Pics = 0  # '图片推荐'
    Comics = 1  # '漫画推荐'
    Words = 2  # '文字推荐'


class EnumRecommendPos(Enum):
    # 组成
    # 1 - pc  2 - h5
    # 1 - banner   2 - 漫画推荐   3 - 文字推荐
    # 001 - 界面
    # 01 - 位置

    WebPcHomeBanners = 1100101  # PC_首页_Banner        1001
    WebPcHomeEditors = 1200102  # PC_首页_编辑推荐       1002
    WebPcHomeWords = 1300103  # PC_首页_文字推荐         1003
    WebPcFilterComics = 1200201  # PC_筛选_漫画推荐      1101
    WebPcDetailComics = 1200301  # PC_详情_漫画推荐      1201
    WebPcReadComics = 1200401  # PC_阅读_漫画推荐        1301
    WebH5HomeBanners = 2100101  # H5_首页_Banner        2001
    WebH5HomeEditors = 2200202  # H5_首页_漫画推荐       2002
    WebH5SearchEditors = 2200301  # H5_搜索_漫画推荐     2101


class EnumLanguageType(Enum):
    en = 0  # 'english'
    zh_hans = 1  # '中文简体'
    zh_hant = 2  # '中文繁體'


class EnumComicRestrict(Enum):
    Normal = 0  # 一般向
    Sensitive = 1  # 擦边级
    Bare = 2  # 裸露级
    Erotic = 3  # 情色级
    Bloody = 4  # 血腥级


class EnumComicReClass(Enum):
    Manga = 1  # 精品
    Doujinshi = 2  # 一般


class EnumComicLevel(Enum):
    High = 0  # 精品
    Normal = 1  # 一般
    Rubbish = 2  # 垃圾


class EnumComicCo(Enum):
    Not_Copyright = 0  # 无版权
    Copyright = 1  # 有版权
    Torts = 2  # 已侵权
    App_Torts = 3  # App侵权


class EnumComicRegion(Enum):
    Japan = 0  # 日本
    Korea = 1  # 韩国
    West = 2  # 欧美
    HK = 3  # 港台
    Inland = 4  # 内地
    Other = 5  # 其他


class EnumComicStatus(Enum):
    Serializing = 0  # 连载中
    Finished = 1  # 已完结
    Short = 2  # 短篇


class EnumComicAudience(Enum):
    Boy = 0  # 少年漫画
    Girl = 1  # 少女漫画
    Male = 2  # 青年漫画
    Female = 3  # 女青漫画


class EnumComicReadDir(Enum):
    No = 0  # 无
    RToL = 1  # 从右到左
    LToR = 2  # 从左到右
    TToB = 3  # 从上到下


class EnumChapterType(Enum):
    Not_Set = 0  # 未设置
    Words = 1  # 话
    Roll = 2  # 卷
    Extra = 3  # 番外篇


class EnumChapterSource(Enum):
    Reprint = 0  # 转载
    Fan = 1  # 同人
    Original = 2  # 原创
    Authorise = 3  # 授权
    Exclusive = 4  # 独家合作
    First = 5  # 首发


class EnumContentSource(Enum):
    HIGH = 10  # 高图
    WIDTH = 20  # 宽图
